package edu.missouristate.repository;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import edu.missouristate.domain.ParkingPermit;

@Repository	
public class ParkingRepository {

	@Autowired
	JdbcTemplate template;

	// Add a BeanPropertyRowMapper here
	BeanPropertyRowMapper<ParkingPermit> PARKING_BPRM = new BeanPropertyRowMapper<ParkingPermit>(ParkingPermit.class);
		
	// Run the query "Select * from parkingpasses" to return a List<ParkingPermit>
	public List<ParkingPermit> getParkingPasses() {
		// Delete the line that reads "return Collections.EMPTY_LIST"
		// Build your own query and return the List<ParkingPermit>
		
		return Collections.EMPTY_LIST;
	}

}
