package edu.missouristate.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="parkingpasses")
public class ParkingPermit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id; // `id` INT NOT NULL,
		
	@Column
	private String firstName; // `first_name` VARCHAR(45) NULL,
	
	@Column
	private String lastName; // `last_name` VARCHAR(45) NULL,
	
	@Column
	private String make; // `make` VARCHAR(45) NULL,
	
	@Column
	private String model; // `model` VARCHAR(45) NULL,
	
	@Column
	private String vehicleYear; // `vehicle_year` VARCHAR(45) NULL,

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getVehicleYear() {
		return vehicleYear;
	}

	public void setVehicleYear(String vehicleYear) {
		this.vehicleYear = vehicleYear;
	}

}
