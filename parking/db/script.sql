CREATE SCHEMA `transportation` ;

CREATE TABLE `transportation`.`parkingpasses` (
  `id` INT NOT NULL,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `make` VARCHAR(45) NULL,
  `model` VARCHAR(45) NULL,
  `vehicle_year` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
  
ALTER TABLE `transportation`.`parkingpasses` 
CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `first_name` `first_name` VARCHAR(45) NOT NULL ,
CHANGE COLUMN `last_name` `last_name` VARCHAR(45) NOT NULL ,
CHANGE COLUMN `make` `make` VARCHAR(45) NOT NULL ,
CHANGE COLUMN `model` `model` VARCHAR(45) NOT NULL ,
CHANGE COLUMN `vehicle_year` `vehicle_year` VARCHAR(45) NOT NULL ;
